package com.abdul.Repository;

import com.abdul.model.Roles;
import com.abdul.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @Author by AbdulQader
 * on 28/11/2018.
 */
public interface RoleRepository extends JpaRepository<Roles,Long> {

    Optional<Roles>findByName(RoleName roleName);
}
