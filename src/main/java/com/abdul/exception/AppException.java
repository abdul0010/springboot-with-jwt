package com.abdul.exception;

/**
 * @Author by AbdulQader
 * on 29/11/2018.
 */
public class AppException extends RuntimeException {

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }
}
