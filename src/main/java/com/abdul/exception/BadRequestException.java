package com.abdul.exception;

/**
 * @Author by AbdulQader
 * on 29/11/2018.
 */
public class BadRequestException extends RuntimeException {
    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
