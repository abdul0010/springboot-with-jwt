package com.abdul.security;

import com.abdul.Repository.UserRepository;
import com.abdul.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author by AbdulQader
 * on 28/11/2018.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService{

    @Autowired
    UserRepository userRepository;
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        User user= userRepository.findByUsernameOrEmail(usernameOrEmail,usernameOrEmail)
                .orElseThrow(()->
                new UsernameNotFoundException("user not found with username or email : "+usernameOrEmail));
    return UserPrincipal.create(user);
    }
    @Transactional
    public UserDetails loadUserById(long id){
        User user=userRepository.findById(id).orElseThrow(()->new UsernameNotFoundException("user not found with this id :"+id));
    return UserPrincipal.create(user);
    }
}
