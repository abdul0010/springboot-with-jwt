package com.abdul.model;

/**
 * @Author by AbdulQader
 * on 28/11/2018.
 */
public enum RoleName {

    ROLE_USER,
    ROLE_ADMIN

}
